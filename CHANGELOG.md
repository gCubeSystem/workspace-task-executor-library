# Changelog for workspace-task-executor-library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.0.2] - 2024-07-17

* Fixing build issue: increasing lower bound version for `data-miner-manager-cl` [#27747#note-11]

## [v1.0.1] - 2021-05-11

#### Fixes

[#21387] Harmonize the jackson version to v.2.8.11
Moved to maven-portal-bom.3.6.2


## [v1.0.0] - 2021-01-18

#### Bug Fixes

[#20457] Fixed incident


## [v0.3.0] - 2020-07-27

[#19677] Migrated to git/jenkins


## [v0.2.0] - 2019-09-09

[Task #17349] Migrate ws-task-executor components to SHUB


## [v0.1.0] - 2018-05-04

First Release
